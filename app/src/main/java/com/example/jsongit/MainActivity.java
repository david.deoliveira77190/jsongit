package com.example.jsongit;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ArrayList<Contact> items;
    ListView lv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lv = (ListView) findViewById(R.id.list);
        items = new ArrayList<Contact>();
    }

    public void onStart() {
        super.onStart();
        // Envoi d'une requete dans la file d'attente
        sendRequest();
    }

    private void sendRequest(){
        StringRequest stringRequest = new StringRequest(SERVER_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("reponse",""+response);
                        parseJSON(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MainActivity.this,error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    public static final String SERVER_URL = "http://192.168.56.1/Json/contacts.json";

    private void parseJSON(String leJson){
        // Traitement de la source Json chargée dans onStart()
        JSONObject jsonResponse;
        ArrayAdapter<Contact> objAdapter;

        try {
            jsonResponse = new JSONObject(leJson);
            // Création du tableau général à partir d'un JSONObject
            JSONArray jsonArray = jsonResponse.getJSONArray("contacts");
            Contact currentFlux = null;
            // Pour chaque élément du tableau
            for (int i = 0; i < jsonArray.length(); i++) {
                currentFlux = new Contact();

                // Création d'un tableau élément à  partir d'un JSONObject
                JSONObject jsonObj = jsonArray.getJSONObject(i);

                // Récupération de l'item qui nous intéresse
                String nom = jsonObj.getString("nom");
                String tel = jsonObj.getString("tel");

                currentFlux.setNom(nom);
                currentFlux.setTel(tel);

                // Ajout dans l'ArrayList
                items.add(currentFlux);
            }

            objAdapter = new ContactAdapter(this, R.layout.row, items);
            lv.setAdapter(objAdapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}