package com.example.jsongit;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class ContactAdapter extends ArrayAdapter<Contact> {

    private Activity activity;
    private List<Contact> items;
    private Contact objBean;
    private int row;

    public ContactAdapter(Activity act, int resource, List<Contact> arrayList) {
        super(act, resource, arrayList);
        this.activity = act;
        this.row = resource;
        this.items = arrayList;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder holder;

        if (view == null) {
            LayoutInflater inflater = (LayoutInflater)
                    activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(row, null);

            holder = new ViewHolder();
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        if ((items == null) || ((position + 1) > items.size()))
            return view;

        objBean = items.get(position);

        holder.nom = (TextView) view.findViewById(R.id.nom);
        holder.tel = (TextView) view.findViewById(R.id.tel);

        if (holder.nom != null && null != objBean.getNom()
                && objBean.getNom().trim().length() > 0) {
            holder.nom.setText(Html.fromHtml(objBean.getNom()));
        }

        if (holder.tel != null && null != objBean.getTel()
                && objBean.getTel().trim().length() > 0) {
            holder.tel.setText(Html.fromHtml(objBean.getTel()));
        }

        return view;
    }

    public class ViewHolder {
        public TextView nom, tel;

        }
}
