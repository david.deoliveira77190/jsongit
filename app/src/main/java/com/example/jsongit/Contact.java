package com.example.jsongit;

public class Contact {
    private String nom = null;
    private String tel = null;

    public Contact() {
        super();
        this.nom = "";
        this.tel = "";
    }

    public Contact(String nom, String tel) {
        super();
        this.nom = nom;
        this.tel = tel;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }
}
